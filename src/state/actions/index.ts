export const addNumber =(number:number,_action_type:string) => ({
    type: 'ADD_NUMBER_TO_DISPLAY',
    number: number,
});

export const executeAction =(action_type:string) => ({
    type: action_type,
});

export const operation = (minArgs:Number, stackUpdater:any, action_type:string) => ({
    minArgs: minArgs,
    stackUpdater: stackUpdater,
    type:action_type,
})

