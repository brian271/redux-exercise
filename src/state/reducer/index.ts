import {operation} from "../actions";

export const rootReducer = (state: { current: string | string[]; stack: any[]; history: string | any[]; } | undefined, action: { type: any; number: any; }) => {
    if (state === undefined) {
        return {
            current: '',
            stack: [],
            history: [{
                current: '',
                stack: [],
            }],
        };
    }

    const calculate_mid_stack = () => {
        if(state.current !== '') {
            return [parseFloat(state.current.toString()) , ...state.stack];
        }
        return state.stack;
    }

    const update_state = (current: string, stack: any[]) => {
        const current_state = {
            current: current,
            stack: stack,
        }
        return {
            current: current_state.current,
            stack: current_state.stack,
            history: [current_state, ...state.history]
        }
    }

    const makeOperation = (operation: { minArgs: any; stackUpdater: any; type?: string; }) => {
        const mid_stack = calculate_mid_stack()
        if (mid_stack.length < operation.minArgs) {
            return state;
        }
        return update_state('', operation.stackUpdater(mid_stack));
    }

    switch (action.type) {
        case 'DO_NOTHING':
            return state;

        case 'ADD_NUMBER_TO_DISPLAY':
            return update_state(state.current + action.number, state.stack);

        case 'INTRO':
            if (state.current !== '') {
                const number_to_stack = state.current;
                return update_state('', [parseFloat(number_to_stack.toString()), ...state.stack]);
            }
            return state;

        case 'SUM':
            const sum = operation(2, (array: string | any[]) => {
                return [array[0]+array[1], ...array.slice(2, array.length)];
            },'SUM')
            return makeOperation(sum);

        case 'SUBTRACT':
            const sub = operation(2, (array: string | any[]) => {
                return [array[0]-array[1], ...array.slice(2, array.length)];
            },'SUBTRACT')
            return makeOperation(sub);

        case 'MULTIPLY':
            const mult = operation(2, (array: string | any[]) => {
                return [array[0]*array[1], ...array.slice(2, array.length)];
            },'MULTIPLY')
            return makeOperation(mult);

        case 'DIV':
            const div = operation(2, (array: string | any[]) => {
                return [array[0]/array[1], ...array.slice(2, array.length)];
            },'DIV')
            return makeOperation(div);

        case 'SUM_ALL':
            const sum_all = operation(2, (array: any[]) => {
                return [array.reduce((a,b) => a+b, 0)];
            }, 'SUM_ALL');
            return makeOperation(sum_all);

        case 'SQR':
            const sqr = operation(1, (array: string | any[]) => {
                return [Math.sqrt(array[0]), ...array.slice(1, array.length)];
            },'SQR')
            return makeOperation(sqr);

        case 'UNDO':
            if (state.history.length > 1) {
                const new_state = state.history[1];
                return {
                    current: new_state.current,
                    stack: new_state.stack,
                    history: state.history.slice(1, state.history.length),
                }
            }
            return state;

        case 'DOT':
            if ((state.current.indexOf('.') === -1) && (state.current !== '')) {
                return update_state(state.current + '.', state.stack);
            }
            return state;

        default:
            return state;
    }
};
