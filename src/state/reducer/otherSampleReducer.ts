import { Reducer } from 'redux';
import { ExampleAction } from '../AppAction';

type OtherSampleState = number;

const initialState: OtherSampleState = 0;

export const otherSampleReducer: Reducer<OtherSampleState, ExampleAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'UPDATE_OTHER_SAMPLE':
      switch (action.function) {
        case 'INCREMENT':
          return state + 1;
        case 'DECREMENT':
          return state - 1;
        default:
          //return assertUnreachable(action.function)
              return 0;
      }
    default:
      return state;
  }
};
